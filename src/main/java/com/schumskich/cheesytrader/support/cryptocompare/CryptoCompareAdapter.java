package com.schumskich.cheesytrader.support.cryptocompare;

import com.schumskich.cheesytrader.support.cryptocompare.transfer.CoinSnapshotResponse;
import com.schumskich.cheesytrader.support.cryptocompare.transfer.TopCoinsResponse;

public interface CryptoCompareAdapter {
    TopCoinsResponse fetchTopCoins();

    CoinSnapshotResponse fetchCoinSnapshot(String symbol);
}
