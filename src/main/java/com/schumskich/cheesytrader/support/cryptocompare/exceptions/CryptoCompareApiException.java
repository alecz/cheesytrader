package com.schumskich.cheesytrader.support.cryptocompare.exceptions;

public class CryptoCompareApiException extends RuntimeException {
    public CryptoCompareApiException(String message) {
        super(message);
    }
}
